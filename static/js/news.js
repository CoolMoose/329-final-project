// News update JS

$(document).ready(function(){
    update()
});

function update() {
    $.get("news")
        .done(function(data) {
            // console.log(data);

            // Insert list into html
            var news = generateList(data);
            $("#newsList").append(news);

        });
}

function generateList(data) {

    var items = [];

    for(var i = 0; i < data.length; i++) {
        var entry = data[i];

        // Title and Link
        var line = '<li><H5>';
        line += entry.title;
        line += ' - <a href="';
        line += entry.url;
        line += '" target="_blank">';
        line += entry.site;
        line += '</a></H5></li>'

        items += line
    }

    return items;
}

