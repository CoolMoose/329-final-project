/**
 * Created by jellison on 11/21/2016.
 */

/*
    This is the module for displaying the digital clock.
*/

$(document).ready(function(){
    var parameters = ["digitalclock", 'Aldrich', 'text', '12hr'];
    run(parameters);
});

/*
    These are default values and should be left alone. If you want to change any of these, change the parameters array up above.
 */
var div = 'd5';
var customFont = 'Aldrich';
var dateFormat = 'text';
var timeFormat = '12hr';

/*
    Runs the program.
    param: params
        An array of strings which is assumed to hold the value of the div, a custom font, date format, and time format.
 */
function run(params){
    if(params[0] !== null && params[0] !== undefined)
        div = params[0];
    if(params[1] !== null && params[1] !== undefined)
        customFont = params[1];
    if(params[2] !== null && params[2] !== undefined)
        dateFormat = params[2];
    if(params[3] !== null && params[3] !== undefined)
        timeFormat = params[3];

    addGoogleFont(customFont);

    displayPiTime();
}

/*
    Displays the time currently set on the Raspberry Pi
 */
function displayPiTime(){
    //display the date and time
    $('.digitalclock').html(
	    getDate(dateFormat) + '<br>' +
        getClockTime(timeFormat)
    );

    //call the displayPiTime() function every second
    var time = setTimeout(displayPiTime, 1000); //calls this function every second
}

/*
    Gets a custom google font dynamically.
 */
function addGoogleFont(FontName) {
    //console.log("Added new font");
    $("head").append("<link href='https://fonts.googleapis.com/css?family=" + FontName + "' rel='stylesheet' type='text/css'>");
}

/*
    Returns the date as a string.
    params:
        dateFormat: string, can be either 'text' or 'numeric', otherwise it will return an empty string.
    return:
        string
 */
function getDate(dateFormat){
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    var dmy = "";
    if(dateFormat == 'text'){
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var suffix = ["st", "nd", "rd", "th"];
        dmy = days[date.getDay()];
        dmy += ' ' + months[month] + ' ' + day;
        if(day == 1){
            dmy += suffix[0];
        }
        else if(day == 2){
            dmy += suffix[1];
        }
        else if(day == 3){
            dmy += suffix[2];
        }
        else{
            dmy += suffix[3];
        }
    }
    else if(dateFormat == 'numeric'){
        dmy = day + '/' + month + '/' + year;
    }
    return dmy;
}

/*
    Returns the current time as a string. Default is 24hr clock.
    params:
        timeFormat: string, can be '12hr'
    return:
        string
 */
function getClockTime(timeFormat){
    var date = new Date();
    var hour;

    if(timeFormat == '12hr')
    {
        hour = (date.getHours() % 12) || 12; // show midnight & noon as 12
    }
    else
    {
        hour = date.getHours();
    }

    return (
        ( hour < 10 ? '0' : '') + hour +
        ( date.getMinutes() < 10 ? ':0' : ':') + date.getMinutes() +
        ( date.getHours() < 12 ? ' AM' : ' PM' )
    );
}
