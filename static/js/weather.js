/**
 * Created by Daniel on 12/3/2016.
 */

$(document).ready(function(){
    getWeather();
    setInterval(getWeather,15000);
});

function getWeather(){
    $.post( "weather", {'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {

            $("#weatherStatus").html(data.status);
            $("#weatherTemp").html(Math.round(data.temp.temp) + "&deg;");
            $("#weatherHumidity").html(data.humidity);
            $("#windSpeed").html(data.wind.speed);
            $("#windDir").html(degToCompass(data.wind.deg));


            $("#weatherRefreshing").fadeOut(500);
            $("#weatherResult").delay(500).fadeIn();

        });
}

function degToCompass(num) {
    var val = Math.floor((num / 22.5) + 0.5);
    var arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"];
    return arr[(val % 16)];
}
