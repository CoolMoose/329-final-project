// Your Client ID can be retrieved from your project in the Google
// Developer Console, https://console.developers.google.com
var CLIENT_ID = '280225116209-74vim3u9vsisvh3sgotf21ht5s4oqeal.apps.googleusercontent.com';

var SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];

/**
 * Check if current user has authorized this application.
 */
function checkAuth() {
    gapi.auth.authorize(
        {
            'client_id': CLIENT_ID,
            'scope': SCOPES.join(' '),
            'immediate': true
        }, handleAuthResult);
}

/**
 * Handle response from authorization server.
 *
 * @param {Object} authResult Authorization result.
 */
function handleAuthResult(authResult) {
    var authorizeDiv = document.getElementById('authorize-div');
    if (authResult && !authResult.error) {
        // Hide auth UI, then load client library.
        authorizeDiv.style.display = 'none';
        loadCalendarApi();
    } else {
        // Show auth UI, allowing the user to initiate authorization by
        // clicking authorize button.
        authorizeDiv.style.display = 'inline';
    }
}

/**
 * Initiate auth flow in response to user clicking authorize button.
 *
 * @param {Event} event Button click event.
 */
function handleAuthClick(event) {
    gapi.auth.authorize(
        {client_id: CLIENT_ID, scope: SCOPES, immediate: false},
        handleAuthResult);
    return false;
}

/**
 * Load Google Calendar client library. List upcoming events
 * once client library is loaded.
 */
function loadCalendarApi() {
    gapi.client.load('calendar', 'v3', listUpcomingEvents);
}

/**
 * Print the summary and start datetime/date of the next ten events in
 * the authorized user's calendar. If no events are found an
 * appropriate message is printed.
 */
function listUpcomingEvents() {

    var list = gapi.client.calendar.calendarList.list();

    list.execute(function (resp) {
        var items = resp.items;
        //console.log(items);

        // Get event ids (primary, holidays ISU Basketball, ISU Football)
        // var cals = [{"id": items[0].id, "description": "Events"},
        //     {"id": items[2].id, "description": items[2].summary},
        //     {"id": items[3].id, "description": items[3].summaryOverride},
        //     {"id": items[4].id, "description": items[4].summaryOverride}];

        // Get current date and add a week to get end time
        var today = new Date().toISOString();

        // Add 6 days to get a week from today, and set to 11:59:59.999 of that day
        var weekFromToday = new Date();
        weekFromToday.setDate(weekFromToday.getDate() + 6);
        weekFromToday.setHours(23);
        weekFromToday.setMinutes(59);
        weekFromToday.setSeconds(59);
        weekFromToday.setMilliseconds(999);
        weekFromToday = weekFromToday.toISOString();

        var calID = 'primary';

        // var calID = cals[i].id;
        // var description = cals[i].description;
        //appendPre(description);

        var request = gapi.client.calendar.events.list({
            'calendarId': calID,
            'timeMin': today,
            'timeMax': weekFromToday,
            'showDeleted': false,
            'singleEvents': true,
            'maxResults': 10,
            'orderBy': 'startTime'
        });

        // Make request based on ID
        request.execute(function(resp) {

            var events = resp.items;

            if (events.length > 0) {
                for (var i = 0; i < events.length; i++) {
                    var event = events[i];
                    var date;

                    if(event.start.hasOwnProperty('date')) {
                        date = new Date(event.start.date);
                        appendPre(event.summary + ' - ' + getDay(date));
                    } else if(event.start.hasOwnProperty('dateTime')) {
                        date = new Date(event.start.dateTime);
                        appendPre(event.summary + ' - ' + getDay(date) + ' @ ' + getMyTime(date));
                    }

                }
            }
        });

        // for(var i = 0; i < cals.length; i++) {
        //
        //     var calID = 'primary';
        //
        //     // var calID = cals[i].id;
        //     // var description = cals[i].description;
        //     //appendPre(description);
        //
        //     var request = gapi.client.calendar.events.list({
        //         'calendarId': calID,
        //         'timeMin': today,
        //         'timeMax': weekFromToday,
        //         'showDeleted': false,
        //         'singleEvents': true,
        //         'maxResults': 10,
        //         'orderBy': 'startTime'
        //     });
        //
        //     // Make request based on ID
        //     request.execute(function(resp) {
        //
        //         var events = resp.items;
        //
        //         if (events.length > 0) {
        //             for (var i = 0; i < events.length; i++) {
        //                 var event = events[i];
        //                 var date = new Date(event.start.dateTime);
        //                 appendPre(event.summary + ' - ' + getDay(date) + ' @ ' + getMyTime(date));
        //             }
        //         }
        //     });
        // }
    });
}

function getDay(date) {
    var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

    return days[date.getDay()];
}

function getMyTime(date) {
    var h = (date.getHours() % 12) || 12; // show midnight & noon as 12

    return (
        ( h < 10 ? '0' : '') + h +
        ( date.getMinutes() < 10 ? ':0' : ':') + date.getMinutes() +
        ( date.getHours() < 12 ? ' AM' : ' PM' )
    );
}

/**
 * Append a pre element to the body containing the given message
 * as its text node.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
    var pre = document.getElementById('calendarEvents');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
}
