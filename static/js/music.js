/**
 * Created by Daniel on 12/3/2016.
 */


$("#skipNext").click(function(){
    $.post( "music", { action: "skipNext",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            console.log(data );
        });
});

$("#skipPrev").click(function(){
    $.post( "music", { action: "skipPrev",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            console.log(data );
        });
});

$("#playPause").click(function(){
    $.post( "music", { action: "playPause",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            if(data == "playing"){
                $("#playPause").html("pause");
            } else if(data == "paused"){
                $("#playPause").html("play_arrow");
            }
        });
});

$("#volumeControl").on('input',function(){
    $.post( "music", { action: "volume", vol:$("#volumeControl").val(),'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
        });
});




$("#IFLRadioMenuItem").click(function(){
    playStation('IFL',"Im Feeling Lucky Radio", "menu");
});

$("#queue_music").click(function(){

    $.post( "music", { action: "getQueue",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            var newHtml = "";
            for(var i = 0; i < data.queue.length; i++){
                var track=data.queue[i];

                newHtml +=
                    '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="'+'moveQueue('+i+')'+'"> <div class="col l2">'+
                    '<img src="'+track.albumArtRef[0].url+'" alt="" class="responsive-img"> </div>'+
                    '<div class="col l10"><b style="font-size:large">'+track.title+'</b><br/><span>'+track.artist+'</span></div>'+
                    '</div>';

            }
            $("#queueNowPlaying").html(data.queue[data.index].title);

            $("#listHolder").html(newHtml);
            swapScreens("nowPlaying", "musicQueue");
        });




});
$("#backToNowPlaying").click(function(){
    swapScreens("musicQueue", "nowPlaying");
});
$("#nowPlayingMenu").click(function(){
    swapScreens("nowPlaying", "musicMenu");
});
$("#menuBackBar").click(function(){
    swapScreens("musicMenu", "nowPlaying");
});

$("#radioStationsMenuItem").click(function(){
    $.post( "music", { action: "getStations",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            data.sort(function(a,b){return b.recentTimestamp - a.recentTimestamp});
            var stopIndex = data.length;
            if(stopIndex > 25){stopIndex = 25;}



            var newHtml = "";
            for(var i = 0; i < stopIndex; i ++){
                var station = data[i];
                console.log(station);
                if(!station.deleted){
                    try{
                        newHtml +=
                            '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="'+'playStation(\''+station.id+'\',\''+station.name.replace(/['"]+/g, '')+'\', \'stationList\')'+'"> <div class="col l2">'+
                            '<img src="'+station.compositeArtRefs[0].url+'" alt="" class="responsive-img"> </div>'+
                            '<div class="col l10"><b style="font-size:large">'+station.name+'</b><br/></div>'+
                            '</div>';
                    }catch(Err){
                        try{
                            newHtml +=
                                '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="'+'playStation(\''+station.id+'\',\''+station.name.replace(/['"]+/g, '')+'\', \'stationList\')'+'"> <div class="col l2">'+
                                '<img src="'+station.imageUrls[0].url+'" alt="" class="responsive-img"> </div>'+
                                '<div class="col l10"><b style="font-size:large">'+station.name+'</b><br/></div>'+
                                '</div>';
                        }catch(Err2){

                        }
                    }
                }
            }

            $("#stationListHolder").html(newHtml);

            swapScreens("musicMenu","stationView");

        });
});


$("#searchBtn").click(function(){
    var q = $("#searchInput").val();

    $.post( "music", { action: "search", q:q,'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            console.log(data );

            $("#searchTerm").html("\""+q+"\"");

            if(data.album_hits.length > 0){
                var showCount = data.album_hits.length;
                if(data.album_hits.length > 10){
                    $("#loadMoreAlbums").fadeIn();
                    showCount = 10;
                }

                var newHtml = "";
                for(var i = 0; i < showCount; i ++){
                    try{
                        var res = data.album_hits[i];
                        newHtml +=
                            '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="'+'viewAlbum(\''+res.album.albumId+'\', \'search\')'+'"> <div class="col l2">'+
                            '<img src="'+res.album.albumArtRef+'" alt="" class="responsive-img"> </div>'+
                            '<div class="col l10"><b style="font-size:large">'+res.album.name+'</b><br/><span>'+res.album.albumArtist+'</span></div>'+
                            '</div>';
                    }catch(err){}
                }
                $("#albumResults").html(newHtml);

            }

            if(data.song_hits.length > 0){
                var showCount = data.song_hits.length;
                if(data.song_hits.length > 10){
                    $("#loadMoreSongs").fadeIn();
                    showCount = 10;
                }
                var newHtml = "";
                for(var i = 0; i < showCount; i ++){
                    try{
                        var res = data.song_hits[i];
                        newHtml +=
                            '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="'+'playSong(\''+res.track.nid+'\', \'search\')'+'"> <div class="col l2">'+
                            '<img src="'+res.track.albumArtRef[0].url+'" alt="" class="responsive-img"> </div>'+
                            '<div class="col l8"><b style="font-size:large">'+res.track.title+'</b><br/><span>'+res.track.artist+'</span></div>'+
                            '<div class="col l2"><i class="small material-icons waves-effect waves-light circle" onclick="">radio</i></div></div>';
                    }catch(err){
                    }
                }

                $("#songResults").html(newHtml);

            }

            if(data.station_hits.length > 0){
                var showCount = data.station_hits.length;
                if(data.station_hits.length > 10){
                    $("#loadmorestations").fadeIn();
                    showCount = 10;
                }
                var newHtml = "";
                for(var i = 0; i < showCount; i ++){
                    try{
                        var res = data.station_hits[i];
                        var seed="";
                        var seedType = res.station.stationSeeds[0].seedType;

                        if(seedType == "9"){
                            seed = res.station.stationSeeds[0].curatedStationId
                        }
                        else if(seedType == "3"){
                            seed = res.station.stationSeeds[0].artistId
                        }
                        else if(seedType == "2"){
                            seed = res.station.stationSeeds[0].trackId
                        }
                        else{
                            console.log("Didn't Find Seed", res.station.stationSeeds[0])
                        }

                        newHtml +=
                            '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="'+'playStation(\''+seed+'\',\''+res.station.name+'\', \'search\')'+'"> <div class="col l2">'+
                            '<img src="'+res.station.compositeArtRefs[0].url+'" alt="" class="responsive-img"> </div>'+
                            '<div class="col l10"><b style="font-size:large">'+res.station.name+'</b><br/></div>'+
                            '</div>';
                    }catch(err){}
                }

                $("#stationResults").html(newHtml);

            }


            $("#musicMenu").fadeOut();
            $("#searchResults").fadeIn();
        });
});


function initMusic(){
    $.post( "music", { action: "initMusic",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {

            swapScreens("musicInit", "musicMenu");

            setInterval(updateTrack, 500);

        });
}

function viewAlbum(id, prevScreen){

    $.post( "music", { action: "getAlbum",nid:id,'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {

            console.log("Album: ", data);

            $("#albumViewTitle").html(data.name);
            $("#albumViewArtist").html(data.artist);

            newHtml = "";

            for(var i= 0; i < data.tracks.length; i++){
                var track = data.tracks[i];
                newHtml +=
                    '<div class="row waves-effect waves-light" style="cursor:pointer; padding:3px" onclick="playSong(\''+track.nid+'\', \'album\')">'+
                    '<div class="col l2">'+
                    '<img src="'+track.albumArtRef[0]['url']+'" alt="" class="responsive-img">'+
                    '</div>'+
                    '<div class="col l8"><b style="font-size:large">'+track.title+'</b><br/><span></span></div>'+
                    '</div>';

            }

            $("#albumTracks").html(newHtml);

            $("#albumBack").click(function(){
                swapScreens("albumView","searchResults");
            });

            if(prevScreen == 'search'){
                swapScreens("searchResults", "albumView");
            }
        });
}

function playSong(songId, prevScreen){
    $.post( "music", { action: "playSong",nid:songId,'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            if(prevScreen == "search"){
                swapScreens("searchResults", "nowPlaying");
            }

        });
}

function playStation(stationId, name, prevScreen){
    $.post( "music", { action: "playStation",nid:stationId,name:name,'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            if(prevScreen == "menu"){
                swapScreens("musicMenu", "nowPlaying");

            }
            if(prevScreen == "stationList"){
                swapScreens("stationView", "nowPlaying");
                $("#backToMenu").click(function(){
                    swapScreens("stationView", "musicMenu");
                });
            }
        });
}

function moveQueue(index){
    $.post( "music", { action: "skipInQueue", index:index,'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {

        });
}


function updateTrack(){
    $.post( "music", { action: "songUpdate",'csrfmiddlewaretoken': csrftoken })
        .done(function( data ) {
            console.log(data);
            if(data.playing == true){
                $("#stationTitle").html(data.radio);
                $("#trackTitle").html(data.title);
                $("#artist").html(data.artist);
                $("#albumImage").attr('src',data.art);
                $("#trackTime").html(data.time);
                $("#trackProgress").css('width', data.timePerc * 100 + '%');
                $("#volumeControl").val(data.vol)
            }else{
                console.log("Not Playing")
            }
        });
}

function swapScreens(currentId, newId){
    $("#"+currentId).fadeOut(500);
    $("#"+newId).delay(500).fadeIn(500)
}

