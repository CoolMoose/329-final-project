from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^music', views.music, name='music'),
    url(r'^news', views.news, name='news'),
    url(r'^weather', views.weather, name='weather'),

]
