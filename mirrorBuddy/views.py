from django.shortcuts import render
from django.http import HttpResponse
import json
from gmusicapi import Mobileclient
import vlc
import pyowm
import requests
import xml.etree.ElementTree as ET

api = Mobileclient()
logged_in = 0
instance = vlc.Instance()
player = instance.media_player_new()
isPlaying = False
playingStation = False
stationName = ""
stationID = ""
vlcEvents = player.event_manager()
musicQIndex = 0
musicQ = []
currentSong = 0


def index(request):
    prefs = {'clock':False, 'music':False, 'weather':False, 'news':False, 'calendar':False}
    with open('templates/mirrorBuddy/config.txt') as config_file:
        config = json.load(config_file)
        
    for app in config['apps']:
        if app in prefs:
            prefs[app] = True
        
    return render(request, 'mirrorBuddy/index.html', prefs)


def music(request):
    reqAction = request.POST['action']

    if reqAction == "initMusic":
        global logged_in
        if not api.is_authenticated():
            logged_in = api.login('dan.albers1@gmail.com', 'mhpijsdtjqqobrdz', '1234565458abcdef')
        global instance
        instance = vlc.Instance()
        global player
        player = instance.media_player_new()
        global vlcPlayer
        vlcPlayer = instance.media_list_player_new()
        global player
        global vlcEvents
        vlcEvents = player.event_manager()
        vlcEvents.event_attach(vlc.EventType().MediaPlayerEndReached, SongFinished, 1)

        global musicQ
        musicQ = []
        global musicQIndex
        musicQIndex = 0

        return HttpResponse("Ok", content_type="application/text")

    if reqAction == "playPause":
        if isPlaying:
            retData = "paused"
            player.pause()
        else:
            retData = "playing"
            player.play()

        global isPlaying
        isPlaying = not isPlaying

        return HttpResponse(retData, content_type="application/text")

    if reqAction == "skipNext":
        print "Next"
        global musicQIndex
        if musicQIndex < len(musicQ):
            musicQIndex += 1
            ResumeQueue()

    if reqAction == "skipPrev":
        print "Prev"
        global musicQIndex

        if player.get_time() > 3000 or musicQIndex <= 0:
            print "Resuming Current Song"
            ResumeQueue()
        else:
            print "Moving Back in Queue"
            musicQIndex -= 1
            ResumeQueue()

    if reqAction == "volume":
        vol = request.POST['vol']
        print "Vol: ", vol
        player.audio_set_volume(int(vol))

    if reqAction == "playSong":
        global musicQ
        global musicQIndex
        musicQ = []
        musicQIndex = 0

        nid = request.POST['nid']
        trackData = api.get_track_info(nid)
        musicQ.append(trackData)
        streamUrl = api.get_stream_url(nid)
        newMedia = instance.media_new(streamUrl)
        player.set_media(newMedia)
        player.play()
        global isPlaying
        isPlaying = True

        global currentSong
        currentSong = api.get_track_info(nid)
        global playingStation
        playingStation = False

    if reqAction == "playStation":
        print "Station"
        global playingStation
        playingStation = True
        global stationName
        stationName = request.POST['name']
        nid = request.POST['nid']
        global stationID
        stationID = nid
        stationTracks = api.get_station_tracks(nid)
        global musicQ
        global musicQIndex
        musicQ = []
        musicQIndex = 0

        firstTrack = stationTracks[0]

        musicQ.append(firstTrack)
        streamUrl = api.get_stream_url(firstTrack['nid'])
        newMedia = instance.media_new(streamUrl)
        player.set_media(newMedia)
        player.play()
        global isPlaying
        isPlaying = True

        global currentSong
        currentSong = firstTrack

        for track in stationTracks[1:]:
            musicQ.append(track)

    if reqAction == "getAlbum":
        print "Album"
        albumId = request.POST['nid']
        print albumId
        albumData = api.get_album_info(albumId)
        return HttpResponse(json.dumps(albumData), content_type="application/json")

    if reqAction == "addToQueue":
        global musicQ
        nid = request.POST['nid']
        trackData = api.get_track_info(nid)
        musicQ.append(trackData)

    if reqAction == "search":
        q = request.POST['q']
        return HttpResponse(json.dumps(api.search(q)), content_type="application/json")

    if reqAction == "songUpdate":
        response_data = {}
        if isPlaying:
            response_data['vol'] = player.audio_get_volume()
            response_data['title'] = currentSong['title']
            response_data['radio'] = stationName if playingStation else currentSong['album']
            response_data['artist'] = currentSong['artist']
            response_data['art'] = currentSong['albumArtRef'][0]['url']
            currentTrackTime = player.get_time() / 1000
            trackFullTime = int(currentSong['durationMillis']) / 1000
            currentTrackTimeSeconds = currentTrackTime % 60
            currentTrackTime /= 60
            currentTrackTimeMinutes = currentTrackTime % 60
            trackFullTimeSeconds = trackFullTime % 60
            trackFullTime /= 60
            trackFullTimeMinutes = trackFullTime % 60
            response_data['time'] = "%d:%d/%d:%d" % (currentTrackTimeMinutes, currentTrackTimeSeconds, trackFullTimeMinutes, trackFullTimeSeconds)
            response_data['timePerc'] = float(float(player.get_time())/float(currentSong['durationMillis']))
            response_data['playing'] = True
        else:
            response_data['playing'] = False
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    if reqAction == "getStations":
        return HttpResponse(json.dumps(api.get_all_stations()), content_type="application/json")

    if reqAction == "getQueue":
        response_data = {}
        response_data['index'] = musicQIndex
        response_data['queue'] = musicQ
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    if reqAction == "skipInQueue":
        reqIndex = int(request.POST['index'])
        global musicQIndex
        musicQIndex = reqIndex
        ResumeQueue()

    return HttpResponse("ok", content_type="application/text")

def ResumeQueue():
    global musicQ
    global musicQIndex
    print "QUEUE", musicQIndex, len(musicQ)
    if musicQIndex < len(musicQ) and musicQIndex >= 0:
        print "Moved In Queue"
        nid = musicQ[musicQIndex]['nid']
        global currentSong
        currentSong = musicQ[musicQIndex]
        streamUrl = api.get_stream_url(nid)
        newMedia = instance.media_new(streamUrl)
        player.set_media(newMedia)
        player.play()
    else:
        print "No Play"

    if musicQIndex >= len(musicQ) and playingStation:
        stationTracks = api.get_station_tracks(stationID, recently_played_ids=(q['nid'] for q in musicQ))
        for track in stationTracks:
            musicQ.append(track);
        ResumeQueue()

def SongFinished(self, data):
    print "Song Ended Advancing In Queue"
    global musicQIndex

    if musicQIndex < len(musicQ):
        musicQIndex += 1
        ResumeQueue()
    else:
        if(playingStation):
            stationTracks = api.get_station_tracks(stationID, recently_played_ids=(q['nid'] for q in musicQ))
            for track in stationTracks:
                musicQ.append(track);
            ResumeQueue()


def weather(request):
    owm = pyowm.OWM('0700c941dd45f7ec52b8e60969b8d014')
    observation = owm.weather_at_place('Ames, IA, USA')
    w = observation.get_weather()

    response_data = {'wind': w.get_wind(), 'status': w.get_status(), 'humidity': w.get_humidity(),
                     'temp': w.get_temperature('fahrenheit')}

    return HttpResponse(json.dumps(response_data), content_type="application/json")


def news(request):

    # News url (Google News, US News)
    url = "https://news.google.com/news/feeds?output=rss&q=us"
    querystring = {"output": "rss", "q": "us"}
    response = requests.request("GET", url, params=querystring)

    # Response string
    xmlString = response.content

    # Parese string to XML
    root = ET.fromstring(xmlString)

    # Get info, place in object
    items = []

    for i in range(9, 18, 1):

        title = root[0][i][0].text
        titleSplitList = title.split(" - ")

        items.append({"title": titleSplitList[0], "site": titleSplitList[1], "url": root[0][i][1].text})

        # Add data if needed in future...
        # items.append({"title": titleSplitList[0], "site": titleSplitList[1], "link": root[0][i][1].text, "date": root[0][i][3].text})

    # Return items as JSON object
    return HttpResponse(json.dumps(items), content_type="application/json")
