from django.shortcuts import render
try:
    import RPi.GPIO as gpio
except:
    print("RPi.GPIO was not found. Assuming not running on Pi and ignoring motionsensor.");


#THIS CURRENTLY IS NOT SETUP TO RUN! NEED TO CHANGE URLS OR SOME SHIT THAT I DON'T
#KNOW TO WELL. WILL BE TESTING SOON FOR FUNCTIONALITY.

#sets gpio to use the board numbering
#the alternative is BCM, which the benefits of are lost on this project
gpio.setmode(gpio.BOARD)

#initial sensor pin
sensorInputPin = 8;

screenState = 1; #0 is off, 1 is on

#disables all pins used in this script and reverts the board numbering system
#this should be called at the end to avoid issues
def runCleanup():
    gpio.cleanup()

#set the sensorInputPin to recieve input from the motion sensor
#and send a signal whenever the sensor changes from low to high
#(i.e. a motion was detected)
def setInputPin():
    gpio.setup(sensorInputPin, gpio.IN, pull_up_down =  gpio.PUD_UP);

#this isn't necessary, I used it for testing
def setOutputPin():
    gpio.setup(sensorInputPin, gpio.OUT);

#"closes" a pin by making it input with no pull up or down
def closePin():
    gpio.setup(sensorInputPin, gpio.IN, pull_up_down = 0);

#returns the sensorInputPin (not the data value)
def getSensorPin():
    return sensorInputPin;

#returns the data value sent by the sensor
def getInput():
    return gpio.input(sensorInputPin);

#change the input pin
def setSensorPin(newPin):
    #NOTE: These pins are for the Raspberry Pi B+ ONLY!
    if newPin == 1 or newPin == 2 or newPin == 4 or newPin == 6 or newPin == 9 or newPin == 14 or newPin == 17 or newPin == 20 or newPin == 25 or newPin == 27 or newPin == 28 or newPin == 30 or newPin == 34 or newPin == 39:
        print("WARNING: Attempted to set pin to a non-I/O pin, no change occured.");
        return;
    sensorInputPin = newPin;

#change screen state variable
def changeScreenState():
    if screenState == 0:
        screenState = 1;

    elif screenState == 1:
        screenState = 0;

def run(): #needs to be changed to prioritize detecting motion over detecting screen state, then act accordingly
    while(1):
        if(screenState == 0):
            if(getInput()):
                changeScreenState();
        #elif() #NEEDS TO DETECT IF MONITOR HAS BEEN ON TO LONG WITHOUT ACTION
    call(["/monitorControl", screenState]); #calls a function from the file /motionsensor/monitorcontrol to turn the monitor on and off STILL TESTING
        

