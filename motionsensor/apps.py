from __future__ import unicode_literals

from django.apps import AppConfig


class MotionsensorConfig(AppConfig):
    name = 'motionsensor'
